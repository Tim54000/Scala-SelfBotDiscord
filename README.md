![](https://framagit.org/Tim54000/Scala-SelfBotDiscord/raw/master/icon.png) Scala-SelfBot
=============

![](https://img.shields.io/badge/Version-0.1.1%20--%20ALPHA-yellowgreen.svg?style=flat-square) ![](https://img.shields.io/badge/Needs-Java%208-lightred.svg?style=flat-square) ![](https://img.shields.io/badge/Dependencies-JDA,%20NightConfig,%20Scala,%20Scala--Logging,%20JPastee-lightgrey.svg?style=flat-square)

A small but simple project to create a self-talk Discord in Scala.

> **[!]** Most of the sentences are in French since it was not intended to be on GitLab.


### Soon

 - A ScalaDoc
 - Remove copyrights and add a GPL license
 - Anothers Commands
 - A Wiki

### Creators

 - [Tim54000](https://framagit.org/Tim54000/) (main developer)