package fr.tim54000.discord.ScalaSelfBot

/**
  * Created by Tim54000 on 04/09/2017 for ScalaSelfBot.
  * Copyright (c) 04/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
object Tokens {
  val DISCORD: String = "DiscordAPIKey"
  val PASTEE : String = "Paste.eeAPIKey"
}
