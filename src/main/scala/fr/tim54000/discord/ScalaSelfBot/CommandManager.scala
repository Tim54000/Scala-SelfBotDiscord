package fr.tim54000.discord.ScalaSelfBot

import fr.tim54000.discord.ScalaSelfBot.Commands.{Command, ExecutorTypes}
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.ChannelType
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

import scala.collection.mutable


/**
  * Created by Tim54000 on 06/09/2017 for ScalaSelfBot.
  * Copyright (c) 06/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
object CommandManager {
  private val commands = mutable.HashMap.empty[String, Command]

  def registerCommand(command: Command): Unit = {
    commands.put(command.getName, command)
  }

  def clearCommands: Unit = commands.clear()

  def executeCommand(cmd: String, messageReceivedEvent: MessageReceivedEvent): Unit = {
    val user = messageReceivedEvent.getAuthor
    val msg = messageReceivedEvent.getMessage
    if (!commands.contains(cmd.toLowerCase())) {
      null match {
        case errorWithEmoji if !msg.getChannelType.eq(ChannelType.GROUP) || msg.getGuild.getMember(SelfBot.jda.getSelfUser).hasPermission(Permission.MESSAGE_ADD_REACTION) => msg.addReaction("❌").complete()
        case errorWithMessage if msg.getGuild.getMember(SelfBot.jda.getSelfUser).hasPermission(Permission.MESSAGE_WRITE) => msg.getTextChannel.sendMessage("❌ Erreur : Commande inconnu !").complete()
      }
      return
    }
    val command = commands(cmd.toLowerCase())
    command match {
      case userCommand if command.getExecutorType.equals(ExecutorTypes.USER) => command.onCommand(messageReceivedEvent.getMessage, messageReceivedEvent)
      case owner if user.eq(SelfBot.jda.getSelfUser) =>
        if (!msg.getChannelType.eq(ChannelType.GROUP) || msg.getGuild.getMember(SelfBot.jda.getSelfUser).hasPermission(Permission.MESSAGE_ADD_REACTION)) command.onCommand(messageReceivedEvent.getMessage, messageReceivedEvent)
      case errorWithEmoji if !msg.getChannelType.eq(ChannelType.GROUP) || msg.getGuild.getMember(SelfBot.jda.getSelfUser).hasPermission(Permission.MESSAGE_ADD_REACTION) => msg.addReaction("❌").complete()
      case errorWithMessage if msg.getGuild.getMember(SelfBot.jda.getSelfUser).hasPermission(Permission.MESSAGE_WRITE) => msg.getTextChannel.sendMessage("❌ Erreur : Commande inconnu !").complete()
      case _ => println("Error")
    }
    /*if(!messageReceivedEvent.getAuthor.eq(SelfBot.jda.getSelfUser) && command.getExecutorType.eq(ExecutorTypes.USER))
      commands(cmd).apply.onCommand(messageReceivedEvent.getMessage, messageReceivedEvent)*/
  }
}
