package fr.tim54000.discord.ScalaSelfBot.Events

import fr.tim54000.discord.ScalaSelfBot.SelfBot
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

import scala.util.Random

/**
  * Created by Tim54000 on 04/09/2017 for ScalaSelfBot.
  * Copyright (c) 04/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
class ChatListener extends ListenerAdapter {
  override def onMessageReceived(e: MessageReceivedEvent): Unit = {
    val message = e.getMessage.getContent
    if (message.eq(" ") || message.eq("") ||message.eq("'") || message.isEmpty) return
    if (message.startsWith(":") || message.endsWith(":")) return
    val origMessage = e.getMessage
    val channel = if (origMessage.getGuild != null) origMessage.getGuild.getName.split(" ")(0) + "#" + origMessage.getTextChannel.getName else "?" + "#" + origMessage.getTextChannel.getName
    var prefix = if (e.getAuthor.isBot) "[BOT] " else ""
    SelfBot.tmpMessageList += origMessage
    if (!message.contains("`"))
      SelfBot.logger.info("[CHAT] [" + channel + "] " + prefix + origMessage.getAuthor.getName + "> " + message)

    if (SelfBot.tmpMessageList.size >= 1500)
      SelfBot.pasteMessageLogs
    val rd = new Random()
    rd.nextInt(100) match {
      case a if 0 to 5 contains (a) => SelfBot.spamList += "\"" + message + "\""
      case _ =>
    }

  }
}
