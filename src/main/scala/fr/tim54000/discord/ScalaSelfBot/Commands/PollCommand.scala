package fr.tim54000.discord.ScalaSelfBot.Commands

import fr.tim54000.discord.ScalaSelfBot.SelfBot
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

import scala.collection.mutable.ArrayBuffer

/**
  * Created by Tim54000 on 09/09/2017 for ScalaSelfBot.
  * Copyright (c) 09/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
class PollCommand extends Command {
  name = "poll"
  description = "Create a poll with Emotes"
  executor = ExecutorTypes.OWNER

  override def onCommand(message: Message, event: MessageReceivedEvent): Unit = {
    val args = message.getContent.replaceFirst("§poll ", "").split(":")
    args.length match {
      case correct if correct >= 3 => {
        message.editMessage("Loading").complete()
        val options = ArrayBuffer.empty[String]
        var reply = ":loudspeaker: **Poll**: "+args(0)+"\n"
        var i = 0
        for(o <- args){
          if(i != 0)
            options+=o
          i+=1
        }
        i=0
        for(o <- options) {
          var emote = SelfBot.emoteMap((i+1).toString)
          reply += emote + " : " + o + "\n"
          print(emote)
          try {
            message.addReaction(emote).complete()
          }catch{
            case e : Throwable => e.printStackTrace()
          }
          i+=1
        }
        message.editMessage(reply).complete()
      }
      case _=> message.addReaction("❌").complete()
    }
  }
}
