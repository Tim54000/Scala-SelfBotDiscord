package fr.tim54000.discord.ScalaSelfBot.Commands

import java.awt.Color

import fr.tim54000.discord.ScalaSelfBot.SelfBot
import net.dv8tion.jda.core.entities.{ChannelType, Message}
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.{EmbedBuilder, Permission}

import scala.util.Random

/**
  * Created by Tim54000 on 06/09/2017 for ScalaSelfBot.
  * Copyright (c) 06/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
class SpamCommand extends Command {

  this.name = "spam"
  this.description = "Spam our conversation !"
  this.executor = ExecutorTypes.USER

  override def onCommand(message: Message, event: MessageReceivedEvent): Unit = {
    val ch = message.getTextChannel
    val member = message.getMember
    if (event.getAuthor.getIdLong.equals(222772775373373440L) || event.getAuthor.getIdLong.equals(220425644540952576L)) {
      if (!message.getChannelType.eq(ChannelType.GROUP) || message.getGuild.getMember(SelfBot.jda.getSelfUser).hasPermission(Permission.MESSAGE_ADD_REACTION)) message.addReaction("✔").complete()
      if (ch.canTalk()) {
        val rd = new Random()
        val randomSentence = SelfBot.spamList(rd.nextInt(SelfBot.spamList.length))
        if (member.hasPermission(Permission.MESSAGE_EMBED_LINKS)) {
          ch.sendMessage("$spam").complete()
          ch.sendMessage(new EmbedBuilder(null).setAuthor("TimBot", "https://framagit.org/Tim54000", "http://blog.soat.fr/wp-content/uploads/2015/08/icon-bots-300x227.png")
            .appendDescription("Error trace : %random%".replaceAll("%random%", randomSentence.replaceAll(" ","-").replaceAll("'", "").replaceAll("@","{}")))
            .setColor(Color.RED)
            .setTitle("ERROR")
            .setThumbnail("http://blog.soat.fr/wp-content/uploads/2015/08/icon-bots-300x227.png")
            .setImage("http://www.windowserrorhelps.com/wp-content/uploads/2011/09/Internet-Explorer-Crashes.jpg")
            .setFooter("Error's instant : " + SelfBot.formatHour(SelfBot.getDate), "https://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/12/1450973046wordpress-errors.png").build()).complete()
        } else {
          ch.sendMessage("$spam %random%".replaceAll("%random%", randomSentence)).complete()
        }
      }
    } else if (!message.getChannelType.eq(ChannelType.GROUP) || message.getGuild.getMember(SelfBot.jda.getSelfUser).hasPermission(Permission.MESSAGE_ADD_REACTION)) message.addReaction("❌").complete()
  }
}
