package fr.tim54000.discord.ScalaSelfBot.Commands
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

/**
  * Created by Tim54000 on 09/09/2017 for ScalaSelfBot.
  * Copyright (c) 09/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
class BanMessageCommand extends Command {
  name = "ban"
  description = "Reply a fake ban message !"
  executor = ExecutorTypes.USER

  override def onCommand(message: Message, event: MessageReceivedEvent): Unit = {
    val args = message.getContent.replaceFirst("§ban ", "")
    if(!message.getContent.contains(" ")) message.getTextChannel.sendMessage("<@"+event.getAuthor.getId+"> was banned by a moderator !").submit()
    if(args!= "" && !event.getJDA.getUsersByName(args, true).isEmpty) message.getTextChannel.sendMessage("<@"+event.getJDA.getUsersByName(args, true).get(0).getId+"> was banned by a moderator !").submit()

  }
}
