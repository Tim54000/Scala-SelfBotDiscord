package fr.tim54000.discord.ScalaSelfBot.Commands

import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

/**
  * Created by Tim54000 on 06/09/2017 for ScalaSelfBot.
  * Copyright (c) 06/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
class Command() {
  protected[this] var name: String = ""
  protected[this] var description: String = ""
  protected[this] var executor: ExecutorTypes.ExecutorType = ExecutorTypes.USER

  def getName: String = name

  def getDescription: String = description

  def getExecutorType: ExecutorTypes.ExecutorType = executor

  def onCommand(message: Message, event: MessageReceivedEvent): Unit = {}

}

object ExecutorTypes {

  sealed trait ExecutorType

  case object OWNER extends ExecutorType

  case object USER extends ExecutorType

}