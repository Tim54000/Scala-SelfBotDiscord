package fr.tim54000.discord.ScalaSelfBot.Commands

import fr.tim54000.discord.ScalaSelfBot.SelfBot.{commands, loadSpamConf, logger}
import fr.tim54000.discord.ScalaSelfBot.{CommandManager, SelfBot}
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

/**
  * Created by Tim54000 on 04/09/2017 for ScalaSelfBot.
  * Copyright (c) 04/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
class StopCommand extends Command {

  name = "stop"
  description = "Stop this bot"
  executor = ExecutorTypes.OWNER

  override def onCommand(message: Message, event: MessageReceivedEvent): Unit = {
    message.getTextChannel.sendMessage("Stopping ...").complete()
    SelfBot.logger.info("Stopping ...")
    val jda = SelfBot.jda
    try {
      loadSpamConf
    } catch {
      case e: Throwable => logger.error("Error with Config")
    }
    SelfBot.logger.info("Saving...")
    SelfBot.pasteMessageLogs
    var i = 0
    for (s <- SelfBot.PasteeLogsList) {
      i += 1
      SelfBot.logger.info("Pastee log n°" + i + " : " + s)
    }
    SelfBot.logger.info("Saved !")
    CommandManager.clearCommands
    jda.shutdown()
  }
}

class TestCommand extends Command {

  name = "test"
  description = "Test"
  executor = ExecutorTypes.OWNER

  override def onCommand(message: Message, event: MessageReceivedEvent): Unit = {
    SelfBot.logger.info("Test !")
    message.getTextChannel.sendMessage("Coucou " + event.getMember.getAsMention + " !").complete()
  }
}

class SaveCommand extends Command {

  name = "save"
  description = "Save message logs to Paste.ee"
  executor = ExecutorTypes.OWNER

  override def onCommand(message: Message, event: MessageReceivedEvent): Unit = {
    SelfBot.logger.info("Saving...")
    SelfBot.pasteMessageLogs
    var i = 0
    for (s <- SelfBot.PasteeLogsList) {
      i += 1
      SelfBot.logger.info("Pastee log n°" + i + " : " + s)
    }
    SelfBot.logger.info("Saved !")
    message.getTextChannel.sendMessage((i * 1500) + " new messages saved !").complete()
  }
}

class ReloadCommand extends Command {
  name = "reload"
  description = "Reload commands"
  executor = ExecutorTypes.OWNER

  override def onCommand(message: Message, event: MessageReceivedEvent): Unit = {
    SelfBot.logger.info("Reloading ...")
    try {
      loadSpamConf
    } catch {
      case e: Throwable => e.printStackTrace()
    }
    var msg = message.getTextChannel.sendMessage("Reloading ...").complete()
    CommandManager.clearCommands
    for (c <- commands)
      CommandManager.registerCommand(c)
    CommandManager.registerCommand(new HelpCommand)
    CommandManager.registerCommand(new BanMessageCommand)
    SelfBot.spamList.clear()
    msg.editMessage("Reloaded !").complete()
  }
}

class PingCommand extends Command {
  name = "ping"
  description = "Get your latency !"
  executor = ExecutorTypes.OWNER

  override def onCommand(message: Message, event: MessageReceivedEvent): Unit = {
    message.editMessage("Ping: " + event.getJDA.getPing + "ms").submit()
  }
}

class HelpCommand extends Command{
  name="help"
  description = "*This*"
  executor = ExecutorTypes.USER

  override def onCommand(message: Message, event: MessageReceivedEvent): Unit = {
    message.addReaction("✔").submit()
    var msg = "===========**HELP**===========\n"
    for(c <- SelfBot.commands){
      if(c.getExecutorType.eq(ExecutorTypes.USER))
        msg+="§**"+c.getName+"** : "+c.getDescription+"\n"
      else msg+="§**"+c.getName+"** : *"+c.getDescription+"*\n"
    }
    msg+="===========**E.N.D**==========="
    message.editMessage(msg).complete()
  }
}
