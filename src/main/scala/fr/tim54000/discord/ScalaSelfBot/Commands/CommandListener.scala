package fr.tim54000.discord.ScalaSelfBot.Commands

import fr.tim54000.discord.ScalaSelfBot.SelfBot.logger
import fr.tim54000.discord.ScalaSelfBot.{CommandManager, SelfBot}
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

/**
  * Created by Tim54000 on 04/09/2017 for ScalaSelfBot.
  * Copyright (c) 04/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
class CommandListener extends ListenerAdapter {
  override def onMessageReceived(event: MessageReceivedEvent): Unit = {
    //logger.info(event.getMessage.getContent)
    if (SelfBot.commandLoaded && event.getMessage.getContent.startsWith("§")) {
      logger.info(event.getMessage.getContent.replaceFirst("§", "").split(" ")(0))
      CommandManager.executeCommand(event.getMessage.getContent.replaceFirst("§", "").split(" ")(0), event)
    }
  }
}
