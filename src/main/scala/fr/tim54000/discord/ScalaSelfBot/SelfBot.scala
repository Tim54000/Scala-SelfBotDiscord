package fr.tim54000.discord.ScalaSelfBot

import java.io.File
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util
import java.util.concurrent.Executors
import java.util.{Calendar, Date}

import com.electronwill.nightconfig.core.{Config, ConfigFormat}
import com.electronwill.nightconfig.core.file.FileConfig
import com.typesafe.scalalogging.Logger
import fr.minuskube.pastee.JPastee
import scala.collection.JavaConverters._
import fr.minuskube.pastee.data.{Paste, Section}
import fr.tim54000.discord.ScalaSelfBot.Commands._
import fr.tim54000.discord.ScalaSelfBot.Events.ChatListener
import net.dv8tion.jda.core.JDA.Status
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.{AccountType, JDA, JDABuilder}

import scala.collection.{JavaConverters, mutable}
import scala.collection.mutable.ArrayBuffer

/**
  * Created by Tim54000 on 04/09/2017 for ScalaSelfBot.
  * Copyright (c) 04/09/2017 - All right reserved !
  *
  * @author Tim54000
  * @version 1.0.0
  */
object SelfBot {
  val tmpMessageList = ArrayBuffer.empty[Message]
  val PasteeLogsList = ArrayBuffer.empty[String]
  val commands = ArrayBuffer.empty[Command]
  val executors = Executors.newScheduledThreadPool(2)
  var spamList = ArrayBuffer.empty[String]
  var emoteMap =mutable.Map.empty[String, String]
  var commandLoaded = false
  var jda: JDA = null

  def main(args: Array[String]): Unit = {
    commands += new BanMessageCommand
    commands += new PingCommand
    commands += new PollCommand
    commands += new ReloadCommand
    commands += new SaveCommand
    commands += new SpamCommand
    commands += new StopCommand()
    commands += new TestCommand
    try {
      logger.warn("Creating an JDA instance ...")
      jda = new JDABuilder(AccountType.CLIENT).addEventListener(new ChatListener).addEventListener(new CommandListener).setToken(Tokens.DISCORD).buildAsync()
    } catch {
      case e: Throwable =>
        logger.error("Failed !")
        throw new Exception("Init failed !", e)
    }
    if (jda == null) throw new NullPointerException("Init failed ! (JDA == null)")
    logger.info("Success !")
    logger.info("Listeners loaded !")
    var i = 0
    while (!jda.getStatus.equals(Status.CONNECTED)) {
      if (i >= 4) i = 0
      logger.info("Connecting to Discord " + ("." * i))
      i += 1
      Thread.sleep(2000)
    }
    logger.info("Connected to Discord !")
    try {
      loadSpamConf
    } catch {
      case e: Throwable => logger.error("Error with Config")
    }
    for (c <- commands)
      CommandManager.registerCommand(c)
    commandLoaded = true
    logger.info("Commands loaded !")
  }

  def logger = Logger(formatHour(getDate) + " - " + "SelfBot")

  def getDate = Calendar.getInstance().getTime

  def formatHour(date: Date): String = {
    val format = new SimpleDateFormat("HH:mm:ss")
    format.format(date)
  }

  def loadSpamConf: Unit = {
    val config = FileConfig.of(new File("config.conf"))
    config.load()
    val saved = config.get[util.Collection[String]]("spam")
    try {
      config.set("spam", JavaConverters.asJavaCollection(spamList))
    } catch {
      case e: Throwable => e.printStackTrace()
    }
    config.save()
    config.load()
    emoteMap.clear()
    for(e <- config.get[Config]("emotes").valueMap().asScala.asInstanceOf[mutable.Map[String, String]]){
      emoteMap.put(e._1,e._2)
    }
    JavaConverters.collectionAsScalaIterable(saved).copyToBuffer(spamList)
    config.save()
    config.close()
  }

  def pasteMessageLogs: Boolean = {
    logger.trace("[CHAT] Paste.ee uploading...")
    val pastee = new JPastee(Tokens.PASTEE)
    val sb = StringBuilder.newBuilder
    for (msg <- tmpMessageList) {
      val channel = if (msg.getGuild != null) msg.getGuild.getName.split(" ")(0) + "#" + msg.getTextChannel.getName else "?" + "#" + msg.getTextChannel.getName
      sb.append(msg.getCreationTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).replaceFirst("T", " ") + " | " + channel + " " + (if (msg.getAuthor.isBot) "[BOT] " else "") + msg.getAuthor.getName + " > " + msg.getContent + "\n")
    }
    val paste = Paste.builder().description("RAW MESSAGE LOG! (" + tmpMessageList(0).getCreationTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).replaceFirst("T", " ") + "-" + tmpMessageList(tmpMessageList.size - 1).getCreationTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).replaceFirst("T", " ") + ")")
      .addSection(Section.builder()
        .name("Messages.log")
        .syntax(pastee.getSyntaxFromName("access log").orElse(null))
        .contents(sb.toString())
        .build())
      .encrypted(true)
      .build()

    var rst = pastee.submit(paste)
    if (rst.isSuccess) {
      logger.info("[CHAT] Paste.ee uploaded : " + rst.getLink)
      tmpMessageList.clear()
      PasteeLogsList += rst.getLink
      rst.isSuccess
    } else {
      logger.error(rst.getErrorString)
      rst.isSuccess
    }
  }
}
